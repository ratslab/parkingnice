package fr.gotmyticketteam.gotmyticket;

import android.app.Activity;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.Calendar;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        MainActivity.nextTime.setText(((String.valueOf(hourOfDay).length() != 1) ? "" : "0") + new String(hourOfDay + ":" + ((String.valueOf(minute).length() != 1) ? "" : "0") + minute));
        //Heure du paiement : MainActivity.currentTime.getText().toString()
        //Heure de depart : MainActivity.nextTime.getText().toString()
        //TODO: Check the price online and put it below
        MainActivity.price.setText("price");
    }
}